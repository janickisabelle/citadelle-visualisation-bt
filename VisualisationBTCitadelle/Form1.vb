﻿Imports ADODB
Imports System.IO
Imports System.Data.SqlClient

Public Class frmBom



    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        Dim sConn As String
        If My.Settings.ENV = "PROD" Then
            sConn = "Provider=sqloledb;Data Source=cit-ple-sql-02;Initial Catalog=daxdb;User Id=loginnove;Password=citadelle1ZZ2;"
        Else
            sConn = "Provider=sqloledb;Data Source=cit-ple-sql-02;Initial Catalog=daxdb_AQ;User Id=loginnove;Password=citadelle1ZZ2;"
        End If


        Try
            OpenConn(sConn)
        Catch
            MsgBox("Erreur de connexion au serveur SQL.")
            Exit Sub
        End Try

        LoadBTList()

        lvBom.Columns(0).Width = 250
        lvBom.Columns(1).Width = 600
        lvBom.Columns(2).Width = 100
        lvBom.Columns(3).Width = 150
        lvBom.Columns(4).Width = 150
        lvBom.Columns(5).Width = 150

    End Sub




    Sub LoadBTList()

        Dim oRS As New ADODB.Recordset
        If txtSearch.Text = "" Then
            oRS = openRs("select distinct(prodid) from prodtable  where prodstatus <= 4 order by prodid desc", True)
        Else
            oRS = openRs("select distinct(prodid) from prodtable  where prodstatus <= 4 and prodid like '%" & txtSearch.Text & "%' order by prodid desc", True)
        End If

        lbBT.Items.Clear()
        If Not (oRS.EOF And oRS.BOF) Then
            Do While Not oRS.EOF

                lbBT.Items.Add(oRS("prodid").Value)
                oRS.MoveNext()
            Loop
        End If
    End Sub





    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        LoadBTList()
    End Sub


    Sub LoadBT(sNum As String)
        Dim oRS As ADODB.Recordset
        Dim oRSBom As ADODB.Recordset
        Dim oRSNotes As ADODB.Recordset
        Dim oLVItem As ListViewItem

        lvBom.Items.Clear()
        txtArticle.Text = ""
        txtBT.Text = sNum
        txtDescription.Text = ""
        txtQte.Text = ""
        txtEntrepot.Text = ""
        txtLivraison.Text = ""
        txtInstructions.Text = ""

        Application.DoEvents()

        oRS = openRs("select wo.prodid 'bon de travail', id.inventlocationid 'Entrepot', wo.dlvdate 'livraison', wo.QTYSCHED 'produire', wo.RemainInventPhysical 'produit', wo.ITEMID,it.ITEMNAME, wo.LBAREPACKAGING from prodtable wo inner join inventdim id on wo.INVENTDIMID = id.INVENTDIMID inner join INVENTTABLE it on wo.ITEMID=it.ITEMID where wo.prodid='" & sNum & "' and id.dataareaid='dat'")
        If Not (oRS.EOF And oRS.BOF) Then
            txtArticle.Text = oRS("itemid").Value
            txtBT.Text = sNum
            txtDescription.Text = oRS("itemname").Value
            txtQte.Text = Math.Round(oRS("produire").Value, 4)
            txtEntrepot.Text = oRS("entrepot").Value
            txtLivraison.Text = oRS("livraison").Value

            oRSBom = openRs("select pb.ITEMID, it.ITEMNAME 'name', pb.UNITID, sum(wo.QTYSCHED*pb.SCRAPVAR/100) loss, pb.BOMQTY 'QTEPar', pb.QTYBOMSTUP from PRODBOM pb inner join INVENTTABLE it on pb.ITEMID=it.ITEMID inner join PRODTABLE wo on pb.PRODID = wo.PRODID where pb.PRODID='" & sNum & "' and pb.DATAAREAID = it.DATAAREAID group by pb.ITEMID, it.ITEMNAME, pb.UNITID, pb.BOMQTY, pb.QTYBOMSTUP")
            If Not (oRSBom.EOF And oRSBom.BOF) Then
                Do While Not oRSBom.EOF
                    oLVItem = New ListViewItem(oRSBom("itemid").Value.ToString)
                    'oLVItem = lvBom.Items.Add(oRSBom("itemid").Value)
                    oLVItem.SubItems.Add(oRSBom("name").Value)
                    oLVItem.SubItems.Add(oRSBom("UNITID").Value)
                    oLVItem.SubItems.Add(Math.Round(oRSBom("qtepar").Value, 4))
                    oLVItem.SubItems.Add(Math.Round(oRSBom("Loss").Value, 4))
                    oLVItem.SubItems.Add(Math.Round(oRSBom("QTYBOMSTUP").Value, 4))
                    lvBom.Items.Add(oLVItem)

                    oRSBom.MoveNext()
                Loop
            End If

            lvBom.Columns(0).Width = 250
            lvBom.Columns(1).Width = 600
            lvBom.Columns(2).Width = 100
            lvBom.Columns(3).Width = 150
            lvBom.Columns(4).Width = 150
            lvBom.Columns(5).Width = 150

            oRSNotes = openRs("select dr.notes from DocuRef DR inner join PRODROUTE pr on DR.REFRECID=pr.RECID and DR.REFCOMPANYID = pr.DATAAREAID where dr.REFCOMPANYID ='dat' and pr.prodid='" & sNum & "'")
            If Not (oRSNotes.EOF And oRSNotes.BOF) Then
                If Not IsDBNull(oRSNotes("notes").Value) Then
                    txtInstructions.Text = Replace(oRSNotes("notes").Value, Chr(10), vbCrLf)
                Else
                    txtInstructions.Text = ""
                End If
            End If
        End If



    End Sub

    Sub loadBTImages(btID As String)
        Dim oRSImages As New Recordset


        ListBox1.Items.Clear()
        oRSImages = openRs("select dr.valuerecid , dr.name from DocuRef dr inner join InventTable it on it.RECID = dr.REFRECID inner join PRODBOM pb on pb.itemId = it.ITEMID inner join DocuValue dv on dv.RECID = dr.VALUERECID where pb.PRODID = '" & btID & "' and dr.TYPEID = 'Image BT'", True)
        If Not (oRSImages.EOF And oRSImages.BOF) Then
            Do While Not oRSImages.EOF
                ListBox1.Items.Add(oRSImages("valuerecid").Value & "-" & oRSImages("name").Value)
                oRSImages.MoveNext()
            Loop


        End If

    End Sub

    Private Sub lbBT_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbBT.SelectedIndexChanged
        LoadBT(lbBT.SelectedItem.ToString)
        loadBTImages(lbBT.SelectedItem.ToString)
    End Sub

    Private Sub txtSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearch.KeyDown


    End Sub

    Private Sub txtSearch_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSearch.KeyPress
        If e.KeyChar = ChrW(Keys.Return) Then
            LoadBTList()
        End If
    End Sub


    


    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        LoadImage(ListBox1.SelectedItem.ToString.Substring(0, 10))

    End Sub

    Sub LoadImage(iId As String)
        Dim oRS As New Recordset
        Dim bImage As Byte()
        'Dim iImage As Bitmap

        Dim ms As New IO.MemoryStream

        oRS = openRs("select file_ from docuvalue where recid = " & iId, True)
        bImage = CType(oRS("file_").Value, Byte())

        ms = New MemoryStream(bImage, True)

        ms.Position = 0

        If Dir("c:\temp\tmp.bmp") <> "" Then Kill("c:\temp\tmp.bmp")

        Dim fs As New BinaryWriter(New FileStream("c:\temp\tmp.bmp", FileMode.Create, FileAccess.Write))

        fs.Write(bImage, 0, ms.Length)
        fs.Close()
        'iImage = New Bitmap("tmp.bmp")

        PictureBox1.ImageLocation = "c:\temp\tmp.bmp"




    End Sub
End Class
