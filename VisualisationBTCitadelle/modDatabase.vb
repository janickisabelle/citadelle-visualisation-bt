﻿Imports ADODB

Module modDatabase

    Public g_Conn As New ADODB.Connection
    Public g_connectionString As String


    Public Function OpenConn(sConn As String) As Boolean

        Try
            g_connectionString = sConn
            g_Conn.CommandTimeout = 0

            g_Conn.Open(sConn)
            Return True
        Catch ex As Exception
            Return False
        End Try



    End Function

    Public Function openRs(aSQL As String, Optional bReadonly As Boolean = False) As ADODB.Recordset

        Dim NewRS As Object
        NewRS = CreateObject("ADODB.recordset")



        If bReadonly Then
            NewRS.Open(LCase(aSQL), g_Conn, 3, 1, 1)
        Else
            NewRS.Open(LCase(aSQL), g_Conn, 2, 3, 1)

        End If
        openRs = NewRS


    End Function
End Module
